FROM centos:7
RUN mkdir /1c
ADD . /1c
WORKDIR /1c
# install 1c
RUN rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7 \
    && yum install epel-release -y \
    && yum install ImageMagick fontconfig fontconfig-devel bind-utils -y\
    && yum localinstall ./1c_additional/*.rpm -y \
    && yum localinstall ./1c_distr/1C_Enterprise83*.rpm -y

# add conf directory
RUN mkdir /opt/1C/v8.3/x86_64/conf/
COPY ./logcfg.xml /opt/1C/v8.3/x86_64/conf/
RUN chown usr1cv8:grp1cv8 -R /opt/1C/v8.3/x86_64/conf/

RUN sed -i "1 i #!/bin/bash" /etc/init.d/srv1cv83

ENTRYPOINT ["/1c/entrypoint.sh"]
CMD ["start"]
